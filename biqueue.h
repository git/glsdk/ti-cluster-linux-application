#ifndef __BIQUEUE_H__
#define __BIQUEUE_H__

#include <pthread.h>
#include <semaphore.h>
#include <list.h>

struct biqueue {
	struct list_head src;
	struct list_head dst;
	pthread_mutex_t lock;
	sem_t src_avail;
	struct list_head *current;
};

struct biqueue *bq_init(struct list_head *entries);
struct list_head *bq_next_empty(struct biqueue *bq);
void bq_queue_full(struct biqueue *bq, struct list_head *entry);
struct list_head *bq_next_full(struct biqueue *bq);
void bq_queue_empty(struct biqueue *bq, struct list_head *entry);

#endif

