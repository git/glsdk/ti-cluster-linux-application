#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

#include <drm_mode.h>
#include <drm_fourcc.h>

#include <list.h>
#include <biqueue.h>
#include <drm_util.h>

#define NUM_BUFFERS_PER_LAYER (6)

#if defined (CONFIG_DISPLAY_SPLIT)
extern bool plane_split;
#endif

static struct {
	int fd;
	int connector_id;
	int crtc_id;
	drmModeModeInfo mode;
	uint32_t mode_blob;

	struct {
		struct biqueue *bq;
		int plane_id;
		int width;
		int height;
		int zorder;
		bool opaque;
		int pos_x;
		int pos_y;
	} layers[32];
	int num_layers;
} drm;

int setup_drm(char *dev_fname)
{
	int r;

	drm.fd = open(dev_fname, O_RDWR);
	if(drm.fd < 0) {
		printf("unable to open card %s\n", dev_fname);
		return -1;
	}

	r = drmSetMaster(drm.fd);
	if(r) {
		printf("unable to set master\n");
		goto err1;
	}

	r = drmSetClientCap(drm.fd, DRM_CLIENT_CAP_UNIVERSAL_PLANES, 1);
	if(r) {
		printf("unable to set universal planes\n");
		goto err1;
	}

	r = drmSetClientCap(drm.fd, DRM_CLIENT_CAP_ATOMIC, 1);
	if(r) {
		printf("unable to set atomic\n");
		goto err1;
	}

	return 0;

err1:
	close(drm.fd);
	return -1;
}

static int get_zorder(int fd, uint32_t plane_id)
{
	int i, r;
	drmModeObjectPropertiesPtr props = NULL;

	props = drmModeObjectGetProperties(fd, plane_id, DRM_MODE_OBJECT_PLANE);
	if(!props) {
		printf("drmModeObjectGetProperties for plane %u failed\n", plane_id);
		return -1;
	}

	for(i = 0; i < props->count_props; i++) {
		drmModePropertyPtr prop = drmModeGetProperty(fd, props->props[i]);
		if(!prop) {
			printf("getting prop = %u for plane %u failed\n", props->props[i], plane_id);
			drmModeFreeObjectProperties(props);
			return -1;
		}
		if(strcmp("zpos", prop->name) == 0) {
			r = props->prop_values[i];
			drmModeFreeProperty(prop);
			drmModeFreeObjectProperties(props);
			return r;
		}
		drmModeFreeProperty(prop);

	}
	printf("could not find property zpos for plane = %u\n", plane_id);
	drmModeFreeObjectProperties(props);

	return -1;
}

static int add_property(int fd, drmModeAtomicReqPtr req, uint32_t id, uint32_t type, char *prop_name, uint64_t value)
{
	int i, r;
	drmModeObjectPropertiesPtr props = NULL;
	
	props = drmModeObjectGetProperties(fd, id, type);
	if(!props) {
		printf("drmModeObjectGetProperties for id = %u failed\n", id);
		return -1;
	}

	for(i = 0; i < props->count_props; i++) {
		drmModePropertyPtr prop = drmModeGetProperty(fd, props->props[i]);
		if(!prop) {
			printf("getting prop = %u for id = %u failed\n", props->props[i], id);
			drmModeFreeObjectProperties(props);
			return -1;
		}
		if(strcmp(prop_name, prop->name) == 0) {
			r = drmModeAtomicAddProperty(req, id, props->props[i], value);
			if(r < 0) {
				printf("set property %s for id = %u failed\n", prop_name, id);
			}
			drmModeFreeProperty(prop);
			drmModeFreeObjectProperties(props);
			return r < 0 ? -1 : 0;
		}
		drmModeFreeProperty(prop);
	}

	printf("could not find property %s for id = %u\n", prop_name, id);
	drmModeFreeObjectProperties(props);
	return -1;
}

static struct buffer *drm_new_buffer(uint32_t w, uint32_t h, uint32_t format)
{
	struct drm_mode_destroy_dumb close_req;
	struct drm_mode_create_dumb create_req;
	struct drm_mode_map_dumb map_req;
	struct buffer *buf;
	void *vaddr;
	uint32_t fb_id;
	int r;

	buf = calloc(sizeof(*buf), 1);
	if(!buf) {
		printf("unable to calloc buffer\n");
		return NULL;
	}

	memset(&create_req, 0, sizeof(create_req));
	create_req.width = w;
	create_req.height =h;
	create_req.bpp = 32;
	r = ioctl(drm.fd, DRM_IOCTL_MODE_CREATE_DUMB, &create_req);
	if(r) {
		printf("unable to create dumb buffer\n");
		goto err1;
	}

	memset(&map_req, 0, sizeof(map_req));
	map_req.handle = create_req.handle;
	r = ioctl(drm.fd, DRM_IOCTL_MODE_MAP_DUMB, &map_req);
	if(r) {
		printf("unable to get map info for dumb buffer\n");
		goto err2;
	}

	vaddr = mmap(0, create_req.size, PROT_READ | PROT_WRITE, MAP_SHARED, drm.fd, map_req.offset);
	if(vaddr == MAP_FAILED) {
		printf("unable to map dumb buffer\n");
		goto err2;
	}

	r = drmModeAddFB(drm.fd, w, h, format == DRM_FORMAT_XRGB8888 ? 24 : 32,
			32, create_req.pitch, create_req.handle, &fb_id);
	if(r) {
		printf("unable to create FB\n");
		exit(-1);
	}

	buf->width = w;
	buf->height = h;
	buf->handle = create_req.handle;
	buf->vaddr = vaddr;
	buf->format = format;
	buf->stride = create_req.pitch;
	buf->fb_id = fb_id;

	return buf;

err2:
	memset(&close_req, 0, sizeof(close_req));
	close_req.handle = create_req.handle;
	ioctl(drm.fd, DRM_IOCTL_MODE_DESTROY_DUMB, &close_req);
err1:
	free(buf);
	return NULL;
}

int drm_setup_display(int w, int h, int *rw, int *rh)
{
	int i, j;
	drmModeResPtr res;
	drmModeConnectorPtr conn = NULL;
	drmModeModeInfoPtr mode = NULL;
	drmModeEncoderPtr enc = NULL;
	drmModeCrtcPtr crtc = NULL;

	res = drmModeGetResources(drm.fd);
	if(!res)
		return -1;

	for(i = 0; i < res->count_connectors; i++) {
		conn = drmModeGetConnector(drm.fd, res->connectors[i]);
		if(!conn)
			return -1;

		for(j = 0; j < conn->count_modes; j++)
			if(conn->modes[j].hdisplay == w && conn->modes[j].vdisplay == h) {
				mode = &conn->modes[j];
				break;
			}

		if(mode)
			break;

		drmModeFreeConnector(conn);
		conn = NULL;
	}

	if(!conn || !mode)
		return -1;

	for(i = 0; i < conn->count_encoders; i++) {
		enc = drmModeGetEncoder(drm.fd, conn->encoders[i]);
		if(!enc)
			return -1;

		for(j = 0; j < res->count_crtcs; j++) {
			int crtc_id;
			uint32_t crtc_mask;

			crtc_id = res->crtcs[j];
			crtc_mask = 1 << j;
			if(enc->possible_crtcs & crtc_mask) {
				crtc = drmModeGetCrtc(drm.fd, res->crtcs[j]);
				if(!crtc)
					return -1;
				break;
			}
		}

		if(crtc)
			break;
	}


	if(!enc || !crtc)
		return -1;


	drm.connector_id = conn->connector_id;
	drm.crtc_id = crtc->crtc_id;
	memcpy(&drm.mode, mode, sizeof(*mode));
	drmModeCreatePropertyBlob(drm.fd, &drm.mode, sizeof(drmModeModeInfo), &drm.mode_blob);

	drmModeFreeConnector(conn);
	drmModeFreeEncoder(enc);
	drmModeFreeCrtc(crtc);
	drmModeFreeResources(res);

	if(rw)
		*rw = drm.mode.hdisplay;
	if(rh)
		*rh = drm.mode.vdisplay;

	return 0;
}

int drm_reset()
{
	int i, r;
	drmModeResPtr res;
	drmModePlaneResPtr pres;
	drmModeAtomicReqPtr disable_req, enable_req;

	res = drmModeGetResources(drm.fd);
	pres = drmModeGetPlaneResources(drm.fd);

	disable_req = drmModeAtomicAlloc();
	enable_req = drmModeAtomicAlloc();

	r = 0;
	for(i = 0; i < res->count_crtcs; i++)
		r += add_property(drm.fd, disable_req, res->crtcs[i], DRM_MODE_OBJECT_CRTC, "ACTIVE", 0);
	for(i = 0; i < pres->count_planes; i++) {
		r += add_property(drm.fd, disable_req, pres->planes[i], DRM_MODE_OBJECT_PLANE, "FB_ID", 0);
		r += add_property(drm.fd, disable_req, pres->planes[i], DRM_MODE_OBJECT_PLANE, "CRTC_ID", 0);
	}
	if(r)
		return -1;

	r = drmModeAtomicCommit(drm.fd, disable_req, DRM_MODE_ATOMIC_ALLOW_MODESET, NULL);
	if(r) {
		printf("disable atomic commit failed\n");
		return -1;
	}

	r = 0;
	r += add_property(drm.fd, enable_req, drm.connector_id, DRM_MODE_OBJECT_CONNECTOR, "CRTC_ID", drm.crtc_id);
	r += add_property(drm.fd, enable_req, drm.crtc_id, DRM_MODE_OBJECT_CRTC, "ACTIVE", 1);
	r += add_property(drm.fd, enable_req, drm.crtc_id, DRM_MODE_OBJECT_CRTC, "MODE_ID", drm.mode_blob);
	if(r)
		return -1;

	r = drmModeAtomicCommit(drm.fd, enable_req, DRM_MODE_ATOMIC_ALLOW_MODESET, NULL);
	if(r) {
		printf("enable atomic commit failed\n");
		exit(-1);
	}

	drmModeAtomicFree(disable_req);
	drmModeAtomicFree(enable_req);

	return 0;
}

static int drm_get_new_plane(int zorder)
{
	int i, j;
	drmModeResPtr res;
	drmModePlaneResPtr pres;
	uint32_t crtc_mask;
	int plane_id;
	drmModePlanePtr plane = NULL;

	pres = drmModeGetPlaneResources(drm.fd);
	res = drmModeGetResources(drm.fd);
	if(!res || !pres)
		return -1;

	for(i = 0; i < res->count_crtcs; i++)
		if(res->crtcs[i] == drm.crtc_id)
			crtc_mask = 1 << i;

	for(i = 0; i < pres->count_planes; i++) {
		plane = drmModeGetPlane(drm.fd, pres->planes[i]);
		if(!plane)
			return -1;

		if((plane->possible_crtcs & crtc_mask) == 0) {
			drmModeFreePlane(plane);
			continue;
		}

		bool occupied = false;
		for(j = 0; j < drm.num_layers; j++)
			if(drm.layers[j].plane_id == plane->plane_id) {
				occupied = true;
				break;
			}

		if(occupied) 
			continue;

		if(get_zorder(drm.fd, plane->plane_id) == zorder)
			break;
	}

	if(!plane)
		return -1;

	plane_id = plane->plane_id;
	drmModeFreePlane(plane);

	return plane_id;
}

int drm_add_layer(int w, int h, int pos_x, int pos_y, int zorder, bool opaque)
{
	int i;
	int count = drm.num_layers;
	struct list_head buffers;


	drm.layers[count].plane_id = drm_get_new_plane(zorder);
	if(drm.layers[count].plane_id < 0) {
		printf("could not get a free plane\n");
		return -1;
	}

	INIT_LIST_HEAD(&buffers);
	for(i = 0; i < NUM_BUFFERS_PER_LAYER; i++) {
		struct buffer *buf = drm_new_buffer(w, h, opaque ? DRM_FORMAT_XRGB8888 : DRM_FORMAT_ARGB8888);
		list_add_tail(&buf->link, &buffers);
	}
	drm.layers[count].bq = bq_init(&buffers);

	drm.layers[count].zorder = zorder;
	drm.layers[count].width = w;
	drm.layers[count].height = h;
	drm.layers[count].pos_x = pos_x;
	drm.layers[count].pos_y = pos_y;
	drm.layers[count].opaque = opaque;

	drm.num_layers++;

	return count;

}

void drm_print_allocations()
{
	int i;

	printf("%s: using connector_id %d\n", __func__, drm.connector_id);
	printf("%s: using crtc_id %d\n", __func__, drm.crtc_id);
	printf("%s: using mode %s\n", __func__, drm.mode.name);
	for(i = 0; i < drm.num_layers; i++)
		printf("%s: using plane_id %d for layer %d\n", __func__, drm.layers[i].plane_id, i);
}

struct biqueue *drm_get_bq(int index)
{
	return drm.layers[index].bq;
}

void *drm_loop(void *arg)
{
	int i, r;
	int count;
	drmModeAtomicReqPtr commit_req;
	struct buffer *old_buffers[32] = {NULL};
	struct buffer *mid_buffers[32] = {NULL};
	struct buffer *new_buffers[32];

	while(true) {
		commit_req = drmModeAtomicAlloc();
		count = drm.num_layers;

		r = 0;
		for(i = 0; i < count; i++) {
			struct buffer *buf = list_entry(bq_next_full(drm.layers[i].bq), struct buffer, link);
			new_buffers[i] = buf;

			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "FB_ID", buf->fb_id);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_ID", drm.crtc_id);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "SRC_X", 0 << 16);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "SRC_Y", 0 << 16);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "SRC_W", buf->width << 16);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "SRC_H", buf->height << 16);
#if defined (CONFIG_DISPLAY_SPLIT)
			if(plane_split == false) {
				r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_X", drm.layers[i].pos_x);
				r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_Y", drm.layers[i].pos_y);
			} else {
				int x, y;
				if(i == 0) {
					x = drm.mode.hdisplay - buf->width;
					y = drm.layers[i].pos_y;
				} else if(i == 1) {
					x = drm.layers[i].pos_x;
					y = drm.mode.vdisplay - buf->height;
				} else if(i == 2) {
					x = drm.mode.hdisplay - buf->width;
					y = drm.mode.vdisplay - buf->height;
				} else {
					x = drm.layers[i].pos_x;
					y = drm.layers[i].pos_y;
				}
				r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_X", x);
				r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_Y", y);
			}
#else
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_X", drm.layers[i].pos_x);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_Y", drm.layers[i].pos_y);
#endif
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_W", drm.layers[i].width);
			r += add_property(drm.fd, commit_req, drm.layers[i].plane_id, DRM_MODE_OBJECT_PLANE, "CRTC_H", drm.layers[i].height);
		}

		if(r) {
			printf("add_property_failed\n");
			goto next_iter;
		}

		r = drmModeAtomicCommit(drm.fd, commit_req, 0, NULL);
		if(r) {
			printf("atomic commit failed\n");
		}
next_iter:
		drmModeAtomicFree(commit_req);

		for(i = 0; i < count; i++) {
			if(old_buffers[i])
				bq_queue_empty(drm.layers[i].bq, &old_buffers[i]->link);
			old_buffers[i] = mid_buffers[i];
			mid_buffers[i] = new_buffers[i];
		}
	}
}

void *drm_buffer_get_priv(struct buffer *b)
{
	return b->priv_data;
}

void drm_buffer_set_priv(struct buffer *b, void *priv)
{
	b->priv_data = priv;
}
