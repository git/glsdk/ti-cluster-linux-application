static struct digit_position dig_pos[] = {
	{
		.name = "digit-0",
		.pos_x = 292,
		.pos_y = 255,
	},
	{
		.name = "digit-1",
		.pos_x = 352,
		.pos_y = 255,
	},
	{
		.name = "digit-2",
		.pos_x = 412,
		.pos_y = 255,
	},
	{
		.name = "digit-0.5",
		.pos_x = 322,
		.pos_y = 255,
	},
	{
		.name = "digit-1.5",
		.pos_x = 382,
		.pos_y = 255,
	},
};
static struct digit_image dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/0-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/1-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/2-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/3-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/4-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/5-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/6-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/7-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/8-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/9-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/space-image-60-77.raw",
		.width = 60,
		.height = 77,
		.value = ' ',
	},
};
static struct digit_position fps_dig_pos[] = {
	{
		.name = "fps-digit-0",
		.pos_x = 1848,
		.pos_y = 1,
	},
	{
		.name = "fps-digit-1",
		.pos_x = 1871,
		.pos_y = 1,
	},
	{
		.name = "fps-digit-2",
		.pos_x = 1894,
		.pos_y = 1,
	},
};
static struct digit_image fps_dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-0-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-1-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-2-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-3-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-4-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-5-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-6-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-7-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-8-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-9-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-space-image-23-30.raw",
		.width = 23,
		.height = 30,
		.value = ' ',
	},
};
