#include <stdlib.h>
#include <drm_util.h>
#include <util.h>
#include <unistd.h>

#include <assets/asset-config.h>

#if defined (CONFIG_DISPLAY_SPLIT)

#if !defined(CONFIG_DISPLAY_WIDTH)
#define CONFIG_DISPLAY_WIDTH (CONFIG_WIDTH * 2)
#endif
#if !defined(CONFIG_DISPLAY_HEIGHT)
#define CONFIG_DISPLAY_HEIGHT (CONFIG_HEIGHT * 2)
#endif

bool plane_split = false;

#else

#if !defined(CONFIG_DISPLAY_WIDTH)
#define CONFIG_DISPLAY_WIDTH (CONFIG_WIDTH)
#endif
#if !defined(CONFIG_DISPLAY_HEIGHT)
#define CONFIG_DISPLAY_HEIGHT (CONFIG_HEIGHT)
#endif

#endif

bool fps_show = true;

int main(int argc, char **argv)
{
	int ret;
	int opt;
	bool keep_going = true;
	int dial_layer_id;
	int needle_layer_id;
	pthread_t drm_thread;
	char *dev_name = CONFIG_DRM_DEVICE_NAME;

	BUILD_BUG_ON(CONFIG_DISPLAY_WIDTH < CONFIG_WIDTH);
	BUILD_BUG_ON(CONFIG_DISPLAY_HEIGHT < CONFIG_HEIGHT);

	while ((opt = getopt(argc, argv, "d:")) != -1) {
		switch (opt) {
		case 'd':
		       dev_name = optarg;
		       break;
		default:
			printf("arguments parsing failed\n");
			return 0;
		}
	}
	ret = setup_drm(dev_name);
	if(ret) {
		printf("setup_drm failed\n");
		return 0;
	}

	ret = drm_setup_display(CONFIG_DISPLAY_WIDTH, CONFIG_DISPLAY_HEIGHT, NULL, NULL);
	if(ret) {
		printf("drm_setup_display failed\n");
		return 0;
	}

	dial_layer_id = drm_add_layer(CONFIG_WIDTH, CONFIG_HEIGHT, 0, 180, 0, true);
	if(dial_layer_id < 0) {
		printf("dial: drm_add_layer failed\n");
		return -1;
	}

	needle_layer_id = drm_add_layer(CONFIG_WIDTH, CONFIG_HEIGHT, 0, 180, 1, false);
	if(needle_layer_id < 0) {
		printf("needle: drm_add_layer failed\n");
		return -1;
	}

	drm_print_allocations();
	drm_reset();

	dial_start(drm_get_bq(dial_layer_id));
	needle_start(drm_get_bq(needle_layer_id));
	pthread_create(&drm_thread, NULL, drm_loop, NULL);

	while(keep_going) {
		char c = getchar();
		switch(c) {
			case 'q':
			case 'Q':
				keep_going = false;
				break;
#if defined (CONFIG_DISPLAY_SPLIT)
			case 't':
			case 'T':
				if(plane_split)
					plane_split = false;
				else
					plane_split = true;
				break;
#endif
			case 'f':
			case 'F':
				if(fps_show)
					fps_show = false;
				else
					fps_show = true;
				break;
		}
	}
}
