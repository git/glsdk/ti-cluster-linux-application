static struct digit_position dig_pos[] = {
	{
		.name = "digit-0",
		.pos_x = 194,
		.pos_y = 170,
	},
	{
		.name = "digit-1",
		.pos_x = 234,
		.pos_y = 170,
	},
	{
		.name = "digit-2",
		.pos_x = 274,
		.pos_y = 170,
	},
	{
		.name = "digit-0.5",
		.pos_x = 214,
		.pos_y = 170,
	},
	{
		.name = "digit-1.5",
		.pos_x = 254,
		.pos_y = 170,
	},
};
static struct digit_image dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/0-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/1-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/2-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/3-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/4-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/5-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/6-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/7-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/8-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/9-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/space-image-40-51.raw",
		.width = 40,
		.height = 51,
		.value = ' ',
	},
};
static struct digit_position fps_dig_pos[] = {
	{
		.name = "fps-digit-0",
		.pos_x = 1232,
		.pos_y = 0,
	},
	{
		.name = "fps-digit-1",
		.pos_x = 1247,
		.pos_y = 0,
	},
	{
		.name = "fps-digit-2",
		.pos_x = 1262,
		.pos_y = 0,
	},
};
static struct digit_image fps_dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-0-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-1-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-2-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-3-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-4-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-5-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-6-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-7-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-8-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-9-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-space-image-15-20.raw",
		.width = 15,
		.height = 20,
		.value = ' ',
	},
};
