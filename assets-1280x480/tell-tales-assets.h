static struct tell_tale_asset tassets[] = {
	{
		.feature = "abs-brake",
		.width = 42,
		.height = 42,
		.pos_x = 289,
		.pos_y = 265,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/abs-brake-289-265-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/abs-brake-289-265-42-42.raw",
	},
	{
		.feature = "airbag-check",
		.width = 42,
		.height = 42,
		.pos_x = 1237,
		.pos_y = 272,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/airbag-check-1237-272-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/airbag-check-1237-272-42-42.raw",
	},
	{
		.feature = "battery",
		.width = 42,
		.height = 42,
		.pos_x = 1157,
		.pos_y = 411,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/battery-1157-411-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/battery-1157-411-42-42.raw",
	},
	{
		.feature = "boot-open",
		.width = 85,
		.height = 42,
		.pos_x = 643,
		.pos_y = 9,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/boot-open-643-9-85-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/boot-open-643-9-85-42.raw",
	},
	{
		.feature = "coil",
		.width = 42,
		.height = 42,
		.pos_x = 0,
		.pos_y = 272,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/coil-0-272-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/coil-0-272-42-42.raw",
	},
	{
		.feature = "door-ajar",
		.width = 42,
		.height = 42,
		.pos_x = 756,
		.pos_y = 9,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/door-ajar-756-9-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/door-ajar-756-9-42-42.raw",
	},
	{
		.feature = "engine",
		.width = 42,
		.height = 42,
		.pos_x = 11,
		.pos_y = 318,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/engine-11-318-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/engine-11-318-42-42.raw",
	},
	{
		.feature = "front-fog-light",
		.width = 42,
		.height = 42,
		.pos_x = 546,
		.pos_y = 432,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/front-fog-light-546-432-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/front-fog-light-546-432-42-42.raw",
	},
	{
		.feature = "front-heat-radiate",
		.width = 42,
		.height = 42,
		.pos_x = 732,
		.pos_y = 432,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/front-heat-radiate-732-432-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/front-heat-radiate-732-432-42-42.raw",
	},
	{
		.feature = "general-warning",
		.width = 42,
		.height = 42,
		.pos_x = 480,
		.pos_y = 9,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/general-warning-480-9-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/general-warning-480-9-42-42.raw",
	},
	{
		.feature = "handbrake-on",
		.width = 42,
		.height = 42,
		.pos_x = 177,
		.pos_y = 265,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/handbrake-on-177-265-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/handbrake-on-177-265-42-42.raw",
	},
	{
		.feature = "high-beam",
		.width = 42,
		.height = 42,
		.pos_x = 596,
		.pos_y = 432,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/high-beam-596-432-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/high-beam-596-432-42-42.raw",
	},
	{
		.feature = "hood-open",
		.width = 85,
		.height = 42,
		.pos_x = 551,
		.pos_y = 9,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/hood-open-551-9-85-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/hood-open-551-9-85-42.raw",
	},
	{
		.feature = "left-indicator",
		.width = 42,
		.height = 42,
		.pos_x = 454,
		.pos_y = 60,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/left-indicator-454-60-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/left-indicator-454-60-42-42.raw",
	},
	{
		.feature = "lights-on",
		.width = 85,
		.height = 42,
		.pos_x = 454,
		.pos_y = 432,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/lights-on-454-432-85-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/lights-on-454-432-85-42.raw",
	},
	{
		.feature = "low-fuel",
		.width = 42,
		.height = 42,
		.pos_x = 1051,
		.pos_y = 264,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/low-fuel-1051-264-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/low-fuel-1051-264-42-42.raw",
	},
	{
		.feature = "oil-level",
		.width = 85,
		.height = 42,
		.pos_x = 935,
		.pos_y = 266,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/oil-level-935-266-85-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/oil-level-935-266-85-42.raw",
	},
	{
		.feature = "rear-heat-radiate",
		.width = 42,
		.height = 42,
		.pos_x = 782,
		.pos_y = 432,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/rear-heat-radiate-782-432-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/rear-heat-radiate-782-432-42-42.raw",
	},
	{
		.feature = "right-indicator",
		.width = 42,
		.height = 42,
		.pos_x = 782,
		.pos_y = 60,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/right-indicator-782-60-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/right-indicator-782-60-42-42.raw",
	},
	{
		.feature = "seat-belt",
		.width = 42,
		.height = 42,
		.pos_x = 1225,
		.pos_y = 318,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/seat-belt-1225-318-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/seat-belt-1225-318-42-42.raw",
	},
	{
		.feature = "steering-wheel-lock",
		.width = 42,
		.height = 42,
		.pos_x = 1201,
		.pos_y = 365,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/steering-wheel-lock-1201-365-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/steering-wheel-lock-1201-365-42-42.raw",
	},
	{
		.feature = "temperature",
		.width = 42,
		.height = 42,
		.pos_x = 79,
		.pos_y = 411,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/temperature-79-411-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/temperature-79-411-42-42.raw",
	},
	{
		.feature = "tyre-pressure",
		.width = 42,
		.height = 42,
		.pos_x = 36,
		.pos_y = 365,
		.on_filename = DATADIR"/"ASSETDIR"/telltales/tyre-pressure-36-365-42-42.raw",
		.off_filename = DATADIR"/"ASSETDIR"/telltales-gray/tyre-pressure-36-365-42-42.raw",
	},
};
