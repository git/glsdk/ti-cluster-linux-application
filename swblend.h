#ifndef __SWBLEND_H__
#define __SWBLEND_H__

#include <stdbool.h>

#define SRC_GL_SRC_ALPHA (0x1)
#define SRC_GL_ONE_MINUS_SRC_ALPHA (0x2)

#define DST_GL_SRC_ALPHA (0x10000)
#define DST_GL_ONE_MINUS_SRC_ALPHA (0x20000)

struct sw_blend_buffer {
	uint32_t width;
	uint32_t height;
	uint32_t format;
	uint32_t stride;
	void *vaddr;
	uint32_t stride2;
	void *vaddr2;
};

struct sw_blend_layer {
	char *name;
	struct sw_blend_buffer buf;
	uint32_t x;
	uint32_t y;
	bool blend;
	uint32_t blendfuncs;
};

struct rect {
	uint32_t left;
	uint32_t top;
	uint32_t width;
	uint32_t height;
};

int sw_blend(struct sw_blend_buffer *dst_buf,
		struct sw_blend_layer *layers, uint32_t layer_cnt,
		bool clear, uint32_t clear_color,
		struct rect *past_damages, int past_dcount,
		struct rect *damages, int dcount);
#endif

