#ifndef __DRM_UTIL__H__
#define __DRM_UTIL_H__

#include <stdint.h>
#include <stdbool.h>

#include <list.h>
#include <biqueue.h>

struct buffer {
	int width;
	int height;
	uint32_t handle;
	void *vaddr;
	uint32_t format;
	int stride;
	uint32_t fb_id;
	void *driver_data;
	void *priv_data;
	struct list_head link;
};

int setup_drm(char *dev_fname);

int drm_setup_display(int w, int h, int *rw, int *rh);
int drm_add_layer(int w, int h, int pos_x, int pos_y, int zorder, bool opaque);
void drm_print_allocations(void);
int drm_reset(void);

struct biqueue *drm_get_bq(int index);
void *drm_loop(void *arg);

void *drm_buffer_get_priv(struct buffer *b);
void drm_buffer_set_priv(struct buffer *b, void *priv);

#endif

