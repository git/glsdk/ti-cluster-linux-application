#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <pthread.h>
#include <semaphore.h>

#include <list.h>
#include <biqueue.h>

struct biqueue *bq_init(struct list_head *entries)
{
	struct list_head *entry, *temp;
	int count;
	struct biqueue *bq;
	
	bq = calloc(sizeof(*bq), 1);
	if(!bq) {
		printf("calloc failed\n");
		return NULL;
	}

	if(list_empty(entries)) {
		printf("cannot initialise biqueue with empty list\n");
		goto err1;
	}
	

	sem_init(&bq->src_avail, 0, 0);
	INIT_LIST_HEAD(&bq->src);
	INIT_LIST_HEAD(&bq->dst);
	bq->current = NULL;
	pthread_mutex_init(&bq->lock, NULL);

	count = 0;
	list_for_each_safe(entry, temp, entries) {
		list_del(entry);
		entry->refcount = 0;

		if(count == 0)
			list_add_tail(entry, &bq->dst);
		else {
			list_add_tail(entry, &bq->src);
			sem_post(&bq->src_avail);
		}
		count++;
	}

	return bq;

err1:
	free(bq);
	return NULL;
}

struct list_head *bq_next_empty(struct biqueue *bq)
{
	struct list_head *entry;

	while(true) {
		sem_wait(&bq->src_avail);

		pthread_mutex_lock(&bq->lock);
		if(list_empty(&bq->src)) {
			pthread_mutex_unlock(&bq->lock);
			continue;
		}

		break;
	}

	entry = bq->src.next;
	list_del(entry);
	pthread_mutex_unlock(&bq->lock);
	
	return entry;
}

void bq_queue_full(struct biqueue *bq, struct list_head *entry)
{
	pthread_mutex_lock(&bq->lock);
	list_add_tail(entry, &bq->dst);
	pthread_mutex_unlock(&bq->lock);
}

struct list_head *bq_next_full(struct biqueue *bq)
{
	struct list_head *entry;

	pthread_mutex_lock(&bq->lock);
	if(list_empty(&bq->dst)) {
		entry = bq->current;
		goto out;
	}

	entry = bq->dst.next;
	list_del(entry);
	bq->current = entry;

out:
	entry->refcount++;
	pthread_mutex_unlock(&bq->lock);
	return entry;
}

void bq_queue_empty(struct biqueue *bq, struct list_head *entry)
{
	entry->refcount--;
	if(!entry->refcount) {
		pthread_mutex_lock(&bq->lock);
		list_add_tail(entry, &bq->src);
		sem_post(&bq->src_avail);
		pthread_mutex_unlock(&bq->lock);
	}
}
