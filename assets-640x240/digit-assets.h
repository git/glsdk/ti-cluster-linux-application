static struct digit_position dig_pos[] = {
	{
		.name = "digit-0",
		.pos_x = 97,
		.pos_y = 85,
	},
	{
		.name = "digit-1",
		.pos_x = 117,
		.pos_y = 85,
	},
	{
		.name = "digit-2",
		.pos_x = 137,
		.pos_y = 85,
	},
	{
		.name = "digit-0.5",
		.pos_x = 107,
		.pos_y = 85,
	},
	{
		.name = "digit-1.5",
		.pos_x = 127,
		.pos_y = 85,
	},
};
static struct digit_image dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/0-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/1-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/2-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/3-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/4-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/5-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/6-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/7-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/8-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/9-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/space-image-20-25.raw",
		.width = 20,
		.height = 25,
		.value = ' ',
	},
};
static struct digit_position fps_dig_pos[] = {
	{
		.name = "fps-digit-0",
		.pos_x = 616,
		.pos_y = 0,
	},
	{
		.name = "fps-digit-1",
		.pos_x = 623,
		.pos_y = 0,
	},
	{
		.name = "fps-digit-2",
		.pos_x = 631,
		.pos_y = 0,
	},
};
static struct digit_image fps_dig_images[] = {
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-0-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '0',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-1-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '1',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-2-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '2',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-3-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '3',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-4-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '4',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-5-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '5',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-6-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '6',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-7-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '7',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-8-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '8',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-9-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = '9',
	},
	{
		.filename = DATADIR"/"ASSETDIR"/digits/fps-space-image-7-10.raw",
		.width = 7,
		.height = 10,
		.value = ' ',
	},
};
